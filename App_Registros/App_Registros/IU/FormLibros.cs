﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App_Registros.IU
{
    public partial class FormLibros : Form
    {
        private FormLibros()
        {
            InitializeComponent();
        }
        private static FormLibros instancia = null;
        public static FormLibros ObtenerInstancia() {
            if (instancia == null) instancia = new FormLibros();
            return instancia;
        }
    }
}
